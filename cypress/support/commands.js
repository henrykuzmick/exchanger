Cypress.Commands.add('getByHook', hook => {
  cy.get(`[data-hook="${hook}"]`);
});

Cypress.Commands.add('pickFromCurrency', i => {
  cy.getByHook('from-pocket-picker').click();
  cy.getByHook(`from-pocket-picker-dropdown-item-${i}`).click();
});

Cypress.Commands.add('pickToCurrency', i => {
  cy.getByHook('to-pocket-picker').click();
  cy.getByHook(`to-pocket-picker-dropdown-item-${i}`).click();
});
