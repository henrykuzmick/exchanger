/// <reference types="Cypress" />

context('Exchanger', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
    cy.server();
    cy.route('/api/latest**', {
      rates: {
        EUR: 0.909069,
        GBP: 0.770194,
        USD: 1,
      },
    });
  });

  it('displays correct exchange values', () => {
    cy.pickFromCurrency(1);
    cy.pickToCurrency(2);
    cy.getByHook('from-price-input').type('1000');
    cy.getByHook('from-price-input').should('have.value', '£1,000');
    cy.getByHook('to-price-input').should('have.value', '$1,298.37');
  });

  it('exchanges value, resets inputs', () => {
    cy.pickFromCurrency(1);
    cy.pickToCurrency(2);

    cy.getByHook('from-pocket-picker').contains('£100');
    cy.getByHook('to-pocket-picker').contains('$0');

    cy.getByHook('from-price-input').type('50');
    cy.getByHook('exchange-button').click();
    cy.getByHook('confirm-button').click();

    cy.getByHook('from-pocket-picker').contains('£50');
    cy.getByHook('to-pocket-picker').contains('$64.91');

    cy.getByHook('from-price-input').should('have.value', '');
    cy.getByHook('to-price-input').should('have.value', '');
  });
});
