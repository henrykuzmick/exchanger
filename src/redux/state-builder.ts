import { State, initialState } from './';
import { Pocket, PocketMap, CURRENCY, Rates } from './types';

export class StateBuilder {
  state: State = initialState;

  withToCurrency(to: CURRENCY) {
    this.state.toCurrency = to;
    return this;
  }

  withFromCurrency(from: CURRENCY) {
    this.state.fromCurrency = from;
    return this;
  }

  withFromAmount(from: string) {
    this.state.fromAmount = from;
    return this;
  }

  withToAmount(to: string) {
    this.state.toAmount = to;
    return this;
  }

  withPockets(pockets: Pocket[]) {
    this.state.pockets = pockets.map(p => p.id);
    this.state.pocketMap = pockets.reduce<PocketMap>((acc, curr) => {
      acc[curr.id] = curr;
      return acc;
    }, {});
    return this;
  }

  withRates(rates: Rates | null) {
    this.state.rates = rates;
    return this;
  }

  build() {
    return this.state;
  }
}
