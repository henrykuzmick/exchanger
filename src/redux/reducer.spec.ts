import { reducer } from './';
import { SET_FROM_CURRENCY, SET_TO_CURRENCY, SET_RATES, EXCHANGE, SET_TO_AMOUNT, SET_FROM_AMOUNT } from './actions';
import { StateBuilder } from './state-builder';
import { CURRENCY } from './types';
import { rates } from '../test/mocks';

describe('Reducer', () => {
  describe(`${SET_RATES}`, () => {
    it('sets rates', () => {
      const state = new StateBuilder().build();
      const result = reducer(state, { type: SET_RATES, payload: rates });
      expect(result).toMatchObject({ rates });
    });
  });

  describe(`${EXCHANGE}`, () => {
    it('correctly exchanges and resets amounts', () => {
      const state = new StateBuilder()
        .withRates(rates)
        .withPockets([
          {
            id: CURRENCY.USD,
            value: 100,
          },
          {
            id: CURRENCY.GBP,
            value: 0,
          },
        ])
        .withFromCurrency(CURRENCY.USD)
        .withToCurrency(CURRENCY.GBP)
        .withFromAmount('$100')
        .build();

      const result = reducer(state, { type: EXCHANGE });
      expect(result).toMatchObject({
        fromAmount: '',
        toAmount: '',
        pocketMap: {
          [CURRENCY.USD]: {
            value: 0,
          },
          [CURRENCY.GBP]: {
            value: 300,
          },
        },
      });
    });
  });

  describe(`${SET_FROM_CURRENCY}`, () => {
    it('sets fromCurrency', () => {
      const state = new StateBuilder().build();
      const result = reducer(state, { type: SET_FROM_CURRENCY, payload: CURRENCY.EUR });
      expect(result).toMatchObject({ fromCurrency: CURRENCY.EUR });
    });

    it('swaps toCurrency and fromCurrency when new fromCurrency === toCurrency', () => {
      const state = new StateBuilder()
        .withToCurrency(CURRENCY.EUR)
        .withFromCurrency(CURRENCY.GBP)
        .withFromAmount('£100')
        .build();

      const result = reducer(state, { type: SET_FROM_CURRENCY, payload: CURRENCY.EUR });
      expect(result).toMatchObject({
        toCurrency: CURRENCY.GBP,
        fromCurrency: CURRENCY.EUR,
      });
    });

    it('updates amounts', () => {
      const state = new StateBuilder()
        .withFromCurrency(CURRENCY.EUR)
        .withFromAmount('€100')
        .withToCurrency(CURRENCY.GBP)
        .withRates(rates)
        .build();
      const result = reducer(state, { type: SET_FROM_CURRENCY, payload: CURRENCY.USD });
      expect(result).toMatchObject({
        fromAmount: '$100',
        toAmount: '£300',
      });
    });
  });

  describe(`${SET_TO_CURRENCY}`, () => {
    it('sets toCurrency', () => {
      const state = new StateBuilder().build();
      const result = reducer(state, { type: SET_TO_CURRENCY, payload: CURRENCY.EUR });
      expect(result).toMatchObject({ toCurrency: CURRENCY.EUR });
    });

    it('swaps toCurrency and fromCurrency when new fromCurrency === toCurrency', () => {
      const state = new StateBuilder()
        .withToCurrency(CURRENCY.EUR)
        .withFromCurrency(CURRENCY.GBP)
        .build();

      const result = reducer(state, { type: SET_TO_CURRENCY, payload: CURRENCY.GBP });
      expect(result).toMatchObject({
        toCurrency: CURRENCY.GBP,
        fromCurrency: CURRENCY.EUR,
      });
    });

    it('updates toAmount', () => {
      const state = new StateBuilder()
        .withFromCurrency(CURRENCY.EUR)
        .withFromAmount('€100')
        .withToCurrency(CURRENCY.GBP)
        .withToAmount('£200')
        .withRates(rates)
        .build();
      const result = reducer(state, { type: SET_TO_CURRENCY, payload: CURRENCY.USD });
      expect(result).toMatchObject({
        toAmount: '$50',
      });
    });
  });

  describe(`${SET_FROM_AMOUNT}`, () => {
    it('updates from and to amounts', () => {
      const state = new StateBuilder()
        .withFromCurrency(CURRENCY.USD)
        .withToCurrency(CURRENCY.GBP)
        .withFromAmount('$10')
        .withRates(rates)
        .build();

      const result = reducer(state, { type: SET_FROM_AMOUNT, payload: '$100' });
      expect(result).toMatchObject({
        fromAmount: '$100',
        toAmount: '£300',
      });
    });
  });

  describe(`${SET_TO_AMOUNT}`, () => {
    it('updates from and to amounts', () => {
      const state = new StateBuilder()
        .withFromCurrency(CURRENCY.USD)
        .withToCurrency(CURRENCY.GBP)
        .withFromAmount('$10')
        .withRates(rates)
        .build();

      const result = reducer(state, { type: SET_TO_AMOUNT, payload: '£100' });
      expect(result).toMatchObject({
        fromAmount: '$33.33',
        toAmount: '£100',
      });
    });
  });
});
