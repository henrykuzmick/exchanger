export enum CURRENCY {
  EUR = 'EUR',
  USD = 'USD',
  GBP = 'GBP',
}

export type Pocket = {
  id: CURRENCY;
  value: number;
};

export type PocketMap = { [key: string]: Pocket };

export type Rates = {
  [key: string]: number;
};

export interface State {
  pockets: string[];
  pocketMap: PocketMap;
  fromCurrency: CURRENCY;
  toCurrency: CURRENCY;
  fromAmount: string;
  toAmount: string;
  rates: Rates | null;
}
