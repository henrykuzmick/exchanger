import { CURRENCY, Rates } from './types';

export const SET_RATES = 'SET_RATES';
export const SET_FROM_CURRENCY = 'SET_FROM_CURRENCY';
export const SET_TO_CURRENCY = 'SET_TO_CURRENCY';
export const SET_FROM_AMOUNT = 'SET_FROM_AMOUNT';
export const SET_TO_AMOUNT = 'SET_TO_AMOUNT';
export const EXCHANGE = 'EXCHANGE';

export interface SetRates {
  type: typeof SET_RATES;
  payload: Rates;
}

export interface SetFromCurrency {
  type: typeof SET_FROM_CURRENCY;
  payload: CURRENCY;
}

export interface SetToCurrency {
  type: typeof SET_TO_CURRENCY;
  payload: CURRENCY;
}

export interface SetFromAmount {
  type: typeof SET_FROM_AMOUNT;
  payload: string;
}

export interface SetToAmount {
  type: typeof SET_TO_AMOUNT;
  payload: string;
}

export interface Exchange {
  type: typeof EXCHANGE;
}

export type Action = SetRates | SetFromCurrency | SetToCurrency | SetFromAmount | SetToAmount | Exchange;
