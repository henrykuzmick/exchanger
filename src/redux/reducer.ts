import { State, CURRENCY } from './types';
import {
  Action,
  SET_FROM_CURRENCY,
  SET_TO_CURRENCY,
  SET_FROM_AMOUNT,
  SET_TO_AMOUNT,
  SET_RATES,
  EXCHANGE,
} from './actions';
import { converter, numericTransformer } from '../utils';

export const initialState: State = {
  pockets: [CURRENCY.EUR, CURRENCY.GBP, CURRENCY.USD],
  pocketMap: {
    [CURRENCY.EUR]: {
      id: CURRENCY.EUR,
      value: 200,
    },
    [CURRENCY.GBP]: {
      id: CURRENCY.GBP,
      value: 100,
    },
    [CURRENCY.USD]: {
      id: CURRENCY.USD,
      value: 0,
    },
  },
  fromCurrency: CURRENCY.EUR,
  toCurrency: CURRENCY.GBP,
  fromAmount: '',
  toAmount: '',
  rates: null,
};

export const reducer = (state = initialState, action: Action): State => {
  switch (action.type) {
    case SET_RATES:
      return {
        ...state,
        rates: action.payload,
      };
    case EXCHANGE: {
      const { pocketMap, fromCurrency, toCurrency, fromAmount, rates } = state;
      const fromPocket = pocketMap[fromCurrency];
      const toPocket = pocketMap[toCurrency];
      const converted = converter({
        fromCurrency,
        toCurrency,
        amount: numericTransformer.parse(fromAmount),
        rates,
      });

      return {
        ...state,
        fromAmount: '',
        toAmount: '',
        pocketMap: {
          ...pocketMap,
          [state.fromCurrency]: {
            ...fromPocket,
            value: fromPocket.value - numericTransformer.parse(state.fromAmount),
          },
          [state.toCurrency]: {
            ...toPocket,
            value: toPocket.value + converted,
          },
        },
      };
    }
    case SET_FROM_CURRENCY: {
      const toCurrency = action.payload === state.toCurrency ? state.fromCurrency : state.toCurrency;
      const converted = converter({
        fromCurrency: action.payload,
        toCurrency,
        amount: numericTransformer.parse(state.fromAmount),
        rates: state.rates,
      });

      return {
        ...state,
        toCurrency,
        fromCurrency: action.payload,
        fromAmount: numericTransformer.transform(state.fromAmount, { currency: action.payload }),
        toAmount: numericTransformer.transform(converted, { currency: toCurrency }),
      };
    }
    case SET_TO_CURRENCY: {
      const fromCurrency = action.payload === state.fromCurrency ? state.toCurrency : state.fromCurrency;
      const converted = converter({
        fromCurrency,
        toCurrency: action.payload,
        amount: numericTransformer.parse(state.fromAmount),
        rates: state.rates,
      });

      return {
        ...state,
        fromCurrency,
        toCurrency: action.payload,
        toAmount: numericTransformer.transform(converted, { currency: action.payload }),
      };
    }
    case SET_FROM_AMOUNT: {
      const { fromCurrency, toCurrency } = state;
      const converted = converter({
        fromCurrency,
        toCurrency,
        amount: numericTransformer.parse(action.payload),
        rates: state.rates,
      });

      return {
        ...state,
        fromAmount: action.payload,
        toAmount: numericTransformer.transform(converted, { currency: toCurrency }),
      };
    }
    case SET_TO_AMOUNT: {
      const { fromCurrency, toCurrency } = state;
      const converted = converter({
        fromCurrency: toCurrency,
        toCurrency: fromCurrency,
        amount: numericTransformer.parse(action.payload),
        rates: state.rates,
      });

      return {
        ...state,
        toAmount: action.payload,
        fromAmount: numericTransformer.transform(converted, { currency: fromCurrency }),
      };
    }
    default:
      return state;
  }
};
