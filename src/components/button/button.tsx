import React from 'react';
import cx from 'classnames';
import s from './button.module.scss';

export interface Props {
  onClick: () => void;
  type?: 'primary' | 'secondary';
  size?: 'normal' | 'large';
  disabled?: boolean;
  dataHook?: string;
}

export const Button: React.FC<Props> = ({
  type = 'secondary',
  size = 'normal',
  onClick,
  disabled,
  dataHook,
  children,
}) => (
  <button
    data-hook={dataHook}
    disabled={disabled}
    className={cx({ [s.button]: true, [s.disabled]: disabled, [s[type]]: true, [s[size]]: true })}
    onClick={onClick}
  >
    {children}
  </button>
);
