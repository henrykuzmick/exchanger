import React from 'react';
import { render } from '../../test/wrapper';
import { Button, Props } from './button';

export const createButtonDriver = (props: Props) => {
  const { getByTestId } = render(
    <Button dataHook="button" {...props}>
      test
    </Button>,
  );

  const click = () => {
    getByTestId('button').click();
  };

  return {
    click,
  };
};
