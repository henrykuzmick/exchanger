import { createButtonDriver } from './button.driver';

describe('Button', () => {
  it('calls onClick when clicked', () => {
    const onClick = jest.fn();
    const driver = createButtonDriver({ onClick });

    driver.click();
    expect(onClick).toHaveBeenCalledTimes(1);
  });

  it('does not call onClick when disabled', () => {
    const onClick = jest.fn();
    const driver = createButtonDriver({ onClick, disabled: true });

    driver.click();
    expect(onClick).not.toHaveBeenCalled();
  });
});
