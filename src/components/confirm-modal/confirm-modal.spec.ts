import { createConfirmModalDriver } from './confirm-modal.driver';
import { StateBuilder, CURRENCY } from '../../redux';
import { rates, pockets } from '../../test/mocks';

const state = new StateBuilder()
  .withRates(rates)
  .withFromCurrency(CURRENCY.USD)
  .withToCurrency(CURRENCY.EUR)
  .withFromAmount('1000')
  .withToAmount('2000')
  .withPockets(pockets)
  .build();

describe('Confirm Modal', () => {
  it('displays correct values', () => {
    const onConfirm = jest.fn();
    const onCancel = jest.fn();
    const driver = createConfirmModalDriver({ onConfirm, onCancel, state });
    expect(driver.getFromValue()).toEqual('$1,000');
    expect(driver.getToValue()).toEqual('€2,000');
  });

  it('calls onConfirm when clicking confirm', () => {
    const onConfirm = jest.fn();
    const onCancel = jest.fn();
    const driver = createConfirmModalDriver({ onConfirm, onCancel, state });
    driver.clickConfirm();
    expect(onConfirm).toHaveBeenCalled();
    expect(onCancel).not.toHaveBeenCalled();
  });

  it('calls onCancel when clicking cancel', () => {
    const onConfirm = jest.fn();
    const onCancel = jest.fn();
    const driver = createConfirmModalDriver({ onConfirm, onCancel, state });
    driver.clickCancel();
    expect(onConfirm).not.toHaveBeenCalled();
    expect(onCancel).toHaveBeenCalled();
  });
});
