import React from 'react';
import Modal from 'react-modal';
import { ArrowRight } from 'react-feather';
import { Button } from '../button';
import { useStore } from '../../hooks';
import { numericTransformer } from '../../utils';
import { Rate } from '../rate';
import s from './confirm-modal.module.scss';

export interface ConfirmModalProps {
  onConfirm: () => void;
  onCancel: () => void;
}

export const ConfirmModal: React.FC<ConfirmModalProps> = props => {
  const { fromCurrency, toCurrency, fromAmount, toAmount } = useStore();

  const fromValue = numericTransformer.transform(fromAmount, { currency: fromCurrency });
  const toValue = numericTransformer.transform(toAmount, { currency: toCurrency });

  return (
    <Modal isOpen className={s.modal} ariaHideApp={false}>
      <div className={s.content} data-hook="confirmation-modal">
        <h2 className={s.heading}>Confirm Exchange?</h2>
        <div className={s.resultWrapper}>
          <div>
            <span className={s.label}>From:</span>
            <h3 className={s.value} data-hook="from-value">
              {fromValue}
            </h3>
          </div>
          <ArrowRight size={24} className={s.arrow} />
          <div>
            <span className={s.label}>To:</span>
            <h3 className={s.value} data-hook="to-value">
              {toValue}
            </h3>
          </div>
        </div>
        <Rate />
      </div>
      <div className={s.actions}>
        <Button dataHook="cancel-button" onClick={props.onCancel}>
          Nevermind
        </Button>
        <Button dataHook="confirm-button" type="primary" onClick={props.onConfirm}>
          Yes, Exchange it!
        </Button>
      </div>
    </Modal>
  );
};
