import React from 'react';
import { render } from '../../test/wrapper';
import { ConfirmModal, ConfirmModalProps } from './confirm-modal';
import { State } from '../../redux';

interface Props extends ConfirmModalProps {
  state: State;
}

export const createConfirmModalDriver = ({ state, ...props }: Props) => {
  const { getByTestId } = render(<ConfirmModal {...props} />, state);

  const getConfirmButton = () => {
    return getByTestId('confirm-button') as HTMLButtonElement;
  };

  const getCancelButton = () => {
    return getByTestId('cancel-button') as HTMLButtonElement;
  };

  const clickConfirm = () => {
    getConfirmButton().click();
  };

  const clickCancel = () => {
    getCancelButton().click();
  };

  const getFromValue = () => {
    return getByTestId('from-value').textContent;
  };

  const getToValue = () => {
    return getByTestId('to-value').textContent;
  };

  return {
    clickConfirm,
    clickCancel,
    getFromValue,
    getToValue,
  };
};
