import { createNotificationDriver } from './notification.driver';
import { useNotification } from '../../hooks';

jest.mock('../../hooks');

describe('Notification', () => {
  it('displays the message', () => {
    const hideNotificationSpy = jest.fn();
    const mockedUseNotification = useNotification as jest.Mocked<any>;
    mockedUseNotification.mockReturnValue({
      hideNotification: hideNotificationSpy,
    });

    const driver = createNotificationDriver({ type: 'danger', message: 'hello' });
    expect(driver.getMessage()).toEqual('hello');
  });

  it('calls closeModal on close button click', () => {
    const hideNotificationSpy = jest.fn();
    const mockedUseNotification = useNotification as jest.Mocked<any>;
    mockedUseNotification.mockReturnValue({
      hideNotification: hideNotificationSpy,
    });

    const driver = createNotificationDriver({ type: 'danger', message: 'hello' });
    driver.clickClose();
    expect(hideNotificationSpy).toHaveBeenCalled();
  });
});
