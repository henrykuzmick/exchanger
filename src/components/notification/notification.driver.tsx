import React from 'react';
import { Notification, NotificationProps } from './notification';
import { render } from '../../test/wrapper';

export const createNotificationDriver = (props: NotificationProps) => {
  const { getByTestId } = render(<Notification {...props} />);

  const getCloseButton = () => {
    return getByTestId('notification-close-button');
  };

  const getMessage = () => {
    return getByTestId('notification-message').textContent;
  };

  const clickClose = () => {
    getCloseButton().click();
  };

  return {
    getMessage,
    clickClose,
  };
};
