import React from 'react';
import { X } from 'react-feather';
import s from './notification.module.scss';
import { useNotification } from '../../hooks';

export type NotificationProps = {
  type: 'danger' | 'success';
  message: string;
};

export const Notification: React.FC<NotificationProps> = props => {
  const { hideNotification } = useNotification();

  return (
    <div className={s.wrapper} data-hook="notification">
      <span data-hook="notification-message" className={s.message}>
        {props.message}
      </span>
      <div data-hook="notification-close-button" className={s.close} onClick={hideNotification}>
        <X size={16} />
      </div>
    </div>
  );
};
