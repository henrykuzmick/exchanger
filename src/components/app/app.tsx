import React from 'react';
import { ContextProvider, ModalProvider, NotificationProvider } from '../../hocs';
import { Loader } from '../';

export const App = () => {
  return (
    <ContextProvider>
      <ModalProvider>
        <NotificationProvider>
          <Loader />
        </NotificationProvider>
      </ModalProvider>
    </ContextProvider>
  );
};
