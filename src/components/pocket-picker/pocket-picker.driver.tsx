import React from 'react';
import { render, fireEvent } from '../../test/wrapper';
import { PocketPicker, Props } from './pocket-picker';
import { State } from '../../redux';
import { DOWN_KEY, ENTER_KEY, UP_KEY, ESC_KEY } from '../../constants';

interface DriverProps extends Props {
  state: State;
}

export const createExchangerDriver = ({ state, ...props }: DriverProps) => {
  const { getByTestId, queryByTestId } = render(<PocketPicker dataHook="pocket-picker" {...props} />, state);

  const getInput = () => {
    return getByTestId('pocket-picker-input') as HTMLInputElement;
  };

  const getInputValue = () => {
    return getInput().value;
  };

  const focus = () => {
    fireEvent.focus(getInput());
  };

  const isOpen = () => {
    return !!queryByTestId(`pocket-picker-dropdown-item-id-0`);
  };

  const getDropdownItemIdText = (i: number) => {
    return getByTestId(`pocket-picker-dropdown-item-id-${i}`).textContent;
  };

  const getDropdownItemValueText = (i: number) => {
    return getByTestId(`pocket-picker-dropdown-item-value-${i}`).textContent;
  };

  const clickOnDropdownItem = (i: number) => {
    const item = getByTestId(`pocket-picker-dropdown-item-${i}`);
    fireEvent.mouseOver(item);
    fireEvent.mouseDown(item);
  };

  const tapDownArrowKey = () => {
    fireEvent.keyDown(document, { keyCode: DOWN_KEY });
  };

  const tapUpArrowKey = () => {
    fireEvent.keyDown(document, { keyCode: UP_KEY });
  };

  const tapEnterKey = () => {
    fireEvent.keyDown(document, { keyCode: ENTER_KEY });
  };

  const tapEscKey = () => {
    fireEvent.keyDown(document, { keyCode: ESC_KEY });
  };

  return {
    getInputValue,
    isOpen,
    clickOnDropdownItem,
    focus,
    getDropdownItemIdText,
    getDropdownItemValueText,
    tapDownArrowKey,
    tapUpArrowKey,
    tapEnterKey,
    tapEscKey,
  };
};
