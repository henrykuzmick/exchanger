import { createExchangerDriver } from './pocket-picker.driver';
import { pockets } from '../../test/mocks';
import { StateBuilder, CURRENCY } from '../../redux';
import { numericTransformer } from '../../utils';

const state = new StateBuilder().withPockets(pockets).build();

describe('Pocket Picker', () => {
  it('displays the selected pocket', () => {
    const driver = createExchangerDriver({ selected: CURRENCY.EUR, onSelect: () => null, state });
    expect(driver.getInputValue()).toEqual(CURRENCY.EUR);
  });

  it('calls onSelect when selected', async () => {
    const onSelect = jest.fn();
    const driver = createExchangerDriver({ selected: CURRENCY.EUR, onSelect, state });

    driver.focus();
    driver.clickOnDropdownItem(1);

    expect(onSelect).toHaveBeenCalledWith(pockets[1].id);
  });

  it('opens the dropdown on focus', () => {
    const onSelect = jest.fn();
    const driver = createExchangerDriver({ selected: CURRENCY.EUR, onSelect, state });
    expect(driver.isOpen()).toBeFalsy();
    driver.focus();
    expect(driver.isOpen()).toBeTruthy();
  });

  it('lists pockets from store', () => {
    const onSelect = jest.fn();
    const driver = createExchangerDriver({ selected: CURRENCY.EUR, onSelect, state });

    driver.focus();
    pockets.forEach((pocket, i) => {
      expect(driver.getDropdownItemIdText(i)).toEqual(pocket.id);
      expect(driver.getDropdownItemValueText(i)).toEqual(
        numericTransformer.transform(pocket.value.toString(), {
          currency: pocket.id,
        }),
      );
    });
  });

  it('selects a pocket using the keyboard down keys', () => {
    const onSelect = jest.fn();
    const driver = createExchangerDriver({ selected: CURRENCY.EUR, onSelect, state });

    driver.focus();
    driver.tapDownArrowKey();
    driver.tapDownArrowKey();
    driver.tapEnterKey();

    expect(onSelect).toHaveBeenCalledWith(pockets[2].id);
  });

  it('selects a pocket using the keyboard up keys', () => {
    const onSelect = jest.fn();
    const driver = createExchangerDriver({ selected: CURRENCY.EUR, onSelect, state });

    driver.focus();
    driver.tapUpArrowKey();
    driver.tapUpArrowKey();
    driver.tapEnterKey();

    expect(onSelect).toHaveBeenCalledWith(pockets[1].id);
  });

  it('closes the driver on ESC tap', () => {
    const onSelect = jest.fn();
    const driver = createExchangerDriver({ selected: CURRENCY.EUR, onSelect, state });

    driver.focus();
    expect(driver.isOpen()).toBeTruthy();

    driver.tapEscKey();
    expect(driver.isOpen()).toBeFalsy();
  });
});
