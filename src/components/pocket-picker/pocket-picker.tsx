import React, { useState, useRef } from 'react';
import cx from 'classnames';
import { ChevronDown } from 'react-feather';
import { usePockets, usePocket, useEventListener } from '../../hooks';
import { DOWN_KEY, UP_KEY, ENTER_KEY, ESC_KEY } from '../../constants';
import { numericTransformer } from '../../utils';
import { CURRENCY } from '../../redux';
import s from './pocket-picker.module.scss';

export interface Props {
  selected: CURRENCY;
  onSelect: (currency: CURRENCY) => void;
  label?: string;
  dataHook?: string;
}

export const PocketPicker: React.FC<Props> = ({ label, selected, onSelect, dataHook }) => {
  const pockets = usePockets();
  const pocket = usePocket(selected);
  const [focused, setFocused] = useState<boolean>(false);
  const [hovered, setHovered] = useState<number>(0);
  const [search, setSearch] = useState<string | null>(null);
  const inputRef = useRef<HTMLInputElement>(null);

  const filteredPockets = pockets.filter(pocket => {
    if (search === null) {
      return true;
    }

    return pocket.id.toLowerCase().includes(search.toLowerCase());
  });

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(e.target.value);
  };

  const onClickItem = () => {
    setFocused(false);
    onSelect(filteredPockets[hovered].id);
    inputRef.current?.blur();
  };

  const onFocus = () => {
    setFocused(true);
    inputRef.current?.select();
  };

  const onBlur = () => {
    setFocused(false);
    setSearch(null);
    inputRef.current?.blur();
  };

  const handleKeyDown = (e: KeyboardEvent) => {
    if (!focused) {
      return;
    }

    switch (e.keyCode) {
      case DOWN_KEY: {
        e.preventDefault();
        if (hovered >= filteredPockets.length - 1) {
          setHovered(0);
        } else {
          setHovered(hovered + 1);
        }
        break;
      }
      case UP_KEY: {
        e.preventDefault();
        if (hovered <= 0) {
          setHovered(filteredPockets.length - 1);
        } else {
          setHovered(hovered - 1);
        }
        break;
      }
      case ENTER_KEY: {
        onClickItem();
        break;
      }
      case ESC_KEY: {
        onBlur();
        break;
      }
      default: {
        setHovered(0);
        break;
      }
    }
  };

  useEventListener('keydown', handleKeyDown);

  const pocketValue = numericTransformer.transform(pocket.value.toString(), { currency: selected });

  return (
    <div data-hook={dataHook} className={s.wrapper} onFocus={onFocus} onClick={onFocus} onBlur={onBlur}>
      <div className={s.picker}>
        <span className={s.label}>{label}</span>
        <div className={s.inputWrapper}>
          <input
            ref={inputRef}
            data-hook={`${dataHook}-input`}
            value={search === null ? selected : search}
            onChange={onChange}
          />
          <span className={s.pocketValue}>{pocketValue}</span>
          <ChevronDown size={16} className={s.chevron} />
        </div>
      </div>
      <div
        data-hook={`${dataHook}-dropdown`}
        className={cx({ [s.dropdownContainer]: true, [s.focused]: focused })}
        onMouseDown={onClickItem}
      >
        {focused &&
          filteredPockets.map((pocket, i) => (
            <div
              key={pocket.id}
              data-hook={`${dataHook}-dropdown-item-${i}`}
              className={cx({ [s.dropdownItem]: true, [s.focused]: i === hovered })}
              onMouseOver={() => setHovered(i)}
            >
              <div data-hook={`${dataHook}-dropdown-item-id-${i}`}>{pocket.id}</div>
              <div data-hook={`${dataHook}-dropdown-item-value-${i}`}>
                {numericTransformer.transform(pocket.value.toString(), { currency: pocket.id })}
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};
