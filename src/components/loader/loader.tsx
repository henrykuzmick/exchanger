import React from 'react';
import { useActions, useStore, useInterval } from '../../hooks';
import { Exchanger } from '../exchanger';
import s from './loader.module.scss';
import { RATE_FETCH_INTERVAL } from '../../constants';

export const Loader = () => {
  const { fetchRates } = useActions();
  const { rates } = useStore();
  useInterval(fetchRates, RATE_FETCH_INTERVAL);

  if (rates === null) {
    return (
      <div data-hook="loader" className={s.wrapper}>
        <div className={s.loader} />
      </div>
    );
  }

  return <Exchanger />;
};
