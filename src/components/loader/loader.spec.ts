import { createLoaderDriver } from './loader.driver';
import { StateBuilder } from '../../redux';
import { ratesService } from '../../services';
import { rates } from '../../test/mocks';

jest.mock('../../services');

const state = new StateBuilder().withRates(null).build();

describe('Loader', () => {
  it('displays a loader while rates === null', () => {
    const driver = createLoaderDriver({ state });
    expect(driver.isLoaderVisible()).toBeTruthy();
  });

  it('displays an error notification when rates thrown an error', async () => {
    const driver = createLoaderDriver({ state });

    const mockedRatesService = ratesService as jest.Mocked<typeof ratesService>;
    mockedRatesService.fetchRates.mockRejectedValue({});

    expect(driver.isLoaderVisible()).toBeTruthy();
    await driver.waitForNotification();
    expect(driver.isNotificationVisible()).toBeTruthy();
  });

  it('displays the exchanger once rates are loaded', async () => {
    const mockedRatesService = ratesService as jest.Mocked<typeof ratesService>;

    mockedRatesService.fetchRates.mockResolvedValue({ data: { rates } });

    const driver = createLoaderDriver({ state });
    await driver.waitForExchanger();
    expect(driver.isExchangerVisible()).toBeTruthy();
  });
});
