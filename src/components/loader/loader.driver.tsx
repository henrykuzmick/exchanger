import React from 'react';
import { State } from '../../redux';
import { render, wait } from '../../test/wrapper';
import { Loader } from './loader';

type Props = {
  state: State;
};

export const createLoaderDriver = ({ state }: Props) => {
  const { queryByTestId } = render(<Loader />, state);

  const isLoaderVisible = () => {
    return !!queryByTestId('loader');
  };

  const isNotificationVisible = () => {
    return !!queryByTestId('notification');
  };

  const isExchangerVisible = () => {
    return !!queryByTestId('exchanger');
  };

  const waitForNotification = async () => {
    await wait(() => isNotificationVisible());
  };

  const waitForExchanger = async () => {
    await wait(() => isExchangerVisible());
  };

  return {
    isLoaderVisible,
    isNotificationVisible,
    isExchangerVisible,
    waitForNotification,
    waitForExchanger,
  };
};
