import React from 'react';
import { useStore } from '../../hooks';
import { numericTransformer, converter } from '../../utils';
import s from './rate.module.scss';

interface Props {
  dataHook?: string;
}

export const Rate: React.FC<Props> = props => {
  const { fromCurrency, toCurrency, rates } = useStore();

  const convertedValue = converter({ fromCurrency, toCurrency, amount: 1, decimals: 5, rates });
  const from = numericTransformer.transform('1', { currency: fromCurrency, thousandSeparator: false });
  const to = numericTransformer.transform(convertedValue, { currency: toCurrency, fixed: 5, thousandSeparator: false });

  return (
    <div className={s.wrapper} data-hook={props.dataHook}>
      {from} = {to}
    </div>
  );
};
