import { createRateDriver } from './rate.driver';
import { StateBuilder, CURRENCY } from '../../redux';
import { rates } from '../../test/mocks';

const state = new StateBuilder()
  .withRates(rates)
  .withFromCurrency(CURRENCY.USD)
  .withToCurrency(CURRENCY.GBP)
  .build();

describe('Rate', () => {
  it('renders correct rate', () => {
    const driver = createRateDriver({ state });
    expect(driver.getValue()).toEqual('$1 = £3');
  });
});
