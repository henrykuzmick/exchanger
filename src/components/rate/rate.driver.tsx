import React from 'react';
import { State } from '../../redux';
import { render } from '../../test/wrapper';
import { Rate } from './rate';

type Props = {
  state: State;
};

export const createRateDriver = ({ state }: Props) => {
  const { getByTestId } = render(<Rate dataHook="rate" />, state);

  const getValue = () => {
    return getByTestId('rate').textContent;
  };

  return {
    getValue,
  };
};
