import React from 'react';
import cx from 'classnames';
import { useStore, useActions, usePocket, useModal } from '../../hooks';
import { PocketPicker, PriceInput, Button } from '../';
import { Rate } from '../rate';
import s from './exchanger.module.scss';
import { numericTransformer } from '../../utils';

export const Exchanger: React.FC = () => {
  const { fromCurrency, toCurrency, fromAmount, toAmount } = useStore();
  const { setFromCurrency, setToCurrency, setFromAmount, setToAmount, exchange } = useActions();
  const fromPocket = usePocket(fromCurrency);
  const { showModal, hideModal } = useModal();

  const onConfirm = () => {
    exchange();
    hideModal();
  };

  const showConfirmationModal = () => showModal({ type: 'confirm', props: { onConfirm, onCancel: hideModal } });

  const parsedAmount = numericTransformer.parse(fromAmount);
  const invalid = fromPocket.value < parsedAmount;

  return (
    <div data-hook="exchanger" className={s.wrapper}>
      <div className={s.side}>
        <PocketPicker label="From" dataHook="from-pocket-picker" selected={fromCurrency} onSelect={setFromCurrency} />
        <div className={s.priceInput}>
          <PriceInput
            dataHook="from-price-input"
            currency={fromCurrency}
            invalid={invalid}
            value={fromAmount}
            onChange={setFromAmount}
          />
        </div>
        <Rate />
      </div>
      <div className={cx([s.darker, s.side])}>
        <PocketPicker label="To" dataHook="to-pocket-picker" selected={toCurrency} onSelect={setToCurrency} />
        <div className={s.priceInput}>
          <PriceInput dataHook="to-price-input" currency={toCurrency} value={toAmount} onChange={setToAmount} />
        </div>
      </div>
      <div className={s.buttonWrapper}>
        <Button
          dataHook="exchange-button"
          type="primary"
          size="large"
          disabled={invalid || parsedAmount < 0.01}
          onClick={showConfirmationModal}
        >
          Exchange
        </Button>
      </div>
    </div>
  );
};
