import { createExchangerDriver } from './exchanger.driver';
import { StateBuilder, CURRENCY } from '../../redux';
import { rates, pockets } from '../../test/mocks';

const state = new StateBuilder()
  .withRates(rates)
  .withFromCurrency(CURRENCY.USD)
  .withToCurrency(CURRENCY.EUR)
  .withPockets(pockets)
  .build();

describe('Exchanger', () => {
  it('displays TO exchange preview when editing FROM value', () => {
    const driver = createExchangerDriver({ state });
    driver.typeFromPrice('10');
    expect(driver.getFromPriceValue()).toEqual('$10');
    expect(driver.getToPriceValue()).toEqual('€20');
  });

  it('displays FROM exchange preview when editing TO value', () => {
    const driver = createExchangerDriver({ state });
    driver.typeToPrice('20');
    expect(driver.getFromPriceValue()).toEqual('$10');
    expect(driver.getToPriceValue()).toEqual('€20');
  });

  it('updates values on pocket change', () => {
    const driver = createExchangerDriver({ state });
    driver.typeFromPrice('10');
    expect(driver.getFromPriceValue()).toEqual('$10');
    expect(driver.getToPriceValue()).toEqual('€20');
    driver.pickFromPocket(2);
    expect(driver.getFromPriceValue()).toEqual('£10');
    expect(driver.getToPriceValue()).toEqual('€6.66');
  });

  it('displays confirmation modal on click', () => {
    const driver = createExchangerDriver({ state });
    expect(driver.isConfirmationModalVisible()).toBeFalsy();
    driver.typeFromPrice('10');
    driver.clickExchange();
    expect(driver.isConfirmationModalVisible()).toBeTruthy();
  });

  it('does not display confirmation modal when the FROM value is 0', () => {
    const driver = createExchangerDriver({ state });
    driver.typeFromPrice('0');
    driver.clickExchange();
    expect(driver.isConfirmationModalVisible()).toBeFalsy();
  });

  it('does not display confirmation modal when the FROM value is too big', () => {
    const driver = createExchangerDriver({ state });
    driver.typeFromPrice('99999');
    driver.clickExchange();
    expect(driver.isConfirmationModalVisible()).toBeFalsy();
  });
});
