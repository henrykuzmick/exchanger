import React from 'react';
import { render, fireEvent } from '../../test/wrapper';
import { Exchanger } from './exchanger';
import { State } from '../../redux';

type Props = {
  state: State;
};

export const createExchangerDriver = (props: Props) => {
  const { getByTestId, queryByTestId } = render(<Exchanger />, props.state);

  const getFromPriceInput = () => {
    return getByTestId('from-price-input') as HTMLInputElement;
  };

  const getToPriceInput = () => {
    return getByTestId('to-price-input') as HTMLInputElement;
  };

  const getExchangeButton = () => {
    return getByTestId('exchange-button') as HTMLButtonElement;
  };

  const typeFromPrice = (value: string) => {
    fireEvent.change(getFromPriceInput(), { target: { value } });
  };

  const typeToPrice = (value: string) => {
    fireEvent.change(getToPriceInput(), { target: { value } });
  };

  const getFromPriceValue = () => {
    return getFromPriceInput().value;
  };

  const getToPriceValue = () => {
    return getToPriceInput().value;
  };

  const pickFromPocket = (i: number) => {
    getByTestId('from-pocket-picker').click();
    const item = getByTestId(`from-pocket-picker-dropdown-item-${i}`);
    fireEvent.mouseOver(item);
    fireEvent.mouseDown(item);
  };

  const clickExchange = () => {
    getExchangeButton().click();
  };

  const isConfirmationModalVisible = () => {
    return !!queryByTestId('confirmation-modal');
  };

  return {
    typeFromPrice,
    typeToPrice,
    getFromPriceValue,
    getToPriceValue,
    pickFromPocket,
    clickExchange,
    isConfirmationModalVisible,
  };
};
