import { createPriceInputDriver } from './price-input.driver';
import { CURRENCY } from '../../redux';

describe('Price Input', () => {
  it('calls onChange with the transformed value', () => {
    const onChange = jest.fn();
    const driver = createPriceInputDriver({ value: '', currency: CURRENCY.USD, onChange });
    driver.type('1000.99');
    expect(onChange).toHaveBeenCalledWith('$1,000.99');
  });

  it('adds invalid class when invalid', () => {
    const driver = createPriceInputDriver({ value: '', currency: CURRENCY.USD, onChange: () => null, invalid: true });
    expect(driver.hasInvalidClass()).toBeTruthy();
  });
});
