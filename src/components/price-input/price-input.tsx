import React from 'react';
import cx from 'classnames';
import { numericTransformer } from '../../utils';
import { CURRENCY } from '../../redux';
import s from './price-input.module.scss';

export interface Props {
  value: string;
  onChange: (value: string) => void;
  currency: CURRENCY;
  invalid?: boolean;
  dataHook?: string;
}

export const PriceInput: React.FC<Props> = ({ value, onChange, currency, invalid, dataHook }) => {
  const onChangeValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = numericTransformer.transform(e.target.value, { currency });
    onChange(newValue);
  };

  const placeholder = numericTransformer.transform('0', { currency });

  return (
    <div>
      <input
        data-hook={dataHook}
        type="text"
        inputMode="decimal"
        placeholder={placeholder}
        className={cx({ [s.input]: true, [s.invalid]: invalid })}
        value={value}
        onChange={onChangeValue}
      />
    </div>
  );
};
