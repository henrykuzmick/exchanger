import React from 'react';
import { render, fireEvent } from '../../test/wrapper';
import { PriceInput, Props } from './price-input';

export const createPriceInputDriver = (props: Props) => {
  const { getByTestId } = render(<PriceInput dataHook="price-input" {...props} />);

  const getInput = () => {
    return getByTestId('price-input') as HTMLInputElement;
  };

  const type = (value: string) => {
    fireEvent.change(getInput(), { target: { value } });
  };

  const getValue = () => {
    return getInput().value;
  };

  const hasInvalidClass = () => {
    return getInput().classList.contains('invalid');
  };

  return {
    type,
    getValue,
    hasInvalidClass,
  };
};
