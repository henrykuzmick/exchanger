import React from 'react';
import { render } from '@testing-library/react';
import { State } from '../redux';
import { ContextProvider, ModalProvider, NotificationProvider } from '../hocs';

type Props = {
  state?: State;
};

const TestWrapper: React.FC<Props> = props => {
  return (
    <ContextProvider state={props.state}>
      <ModalProvider>
        <NotificationProvider>{props.children}</NotificationProvider>
      </ModalProvider>
    </ContextProvider>
  );
};

const customRender = (ui: React.ReactElement, state?: State) => {
  return render(ui, {
    wrapper: ({ children }) => <TestWrapper state={state}>{children}</TestWrapper>,
  });
};

export * from '@testing-library/react';
export { customRender as render };
