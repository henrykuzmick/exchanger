import { CURRENCY, Pocket } from '../redux';

export const rates = {
  [CURRENCY.USD]: 1,
  [CURRENCY.EUR]: 2,
  [CURRENCY.GBP]: 3,
};

export const pockets: Pocket[] = [
  {
    id: CURRENCY.USD,
    value: 100,
  },
  {
    id: CURRENCY.EUR,
    value: 100,
  },
  {
    id: CURRENCY.GBP,
    value: 100,
  },
];
