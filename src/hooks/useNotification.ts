import { useContext } from 'react';
import { NotificationContext } from '../hocs';

export const useNotification = () => {
  const { showNotification, hideNotification } = useContext(NotificationContext);

  return {
    showNotification,
    hideNotification,
  };
};
