import { useStore } from './useStore';

export const usePocket = (id: string) => {
  const { pocketMap } = useStore();
  return pocketMap[id];
};
