import { useStore } from './useStore';

export const usePockets = () => {
  const { pockets, pocketMap } = useStore();
  return pockets.map(id => pocketMap[id]);
};
