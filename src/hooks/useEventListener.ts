import { useRef, useEffect } from 'react';

export const useEventListener = (eventName: keyof DocumentEventMap, handler: any) => {
  const savedHandler = useRef<any>(null);

  useEffect(() => {
    savedHandler.current = handler;
  }, [handler]);

  useEffect(() => {
    const eventListener = (event: Event) => {
      savedHandler.current && savedHandler.current(event);
    };

    document.addEventListener(eventName, eventListener);
    return () => {
      document.removeEventListener(eventName, eventListener);
    };
  }, [eventName]);
};
