import { useContext } from 'react';
import { ReduxContext } from '../hocs';

export const useStore = () => {
  const { state } = useContext(ReduxContext);
  return state;
};
