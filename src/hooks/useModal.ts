import { useContext } from 'react';
import { ModalContext } from '../hocs';

export const useModal = () => {
  const { showModal, hideModal } = useContext(ModalContext);

  return {
    showModal,
    hideModal,
  };
};
