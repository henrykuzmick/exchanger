import { useContext } from 'react';
import { ReduxContext } from '../hocs';
import {
  SET_FROM_CURRENCY,
  SET_TO_CURRENCY,
  CURRENCY,
  SET_FROM_AMOUNT,
  SET_TO_AMOUNT,
  SET_RATES,
  EXCHANGE,
} from '../redux';
import { ratesService } from '../services';
import { useNotification } from './useNotification';

export const useActions = () => {
  const { dispatch } = useContext(ReduxContext);
  const { showNotification } = useNotification();

  const fetchRates = async () => {
    try {
      const res = await ratesService.fetchRates();
      dispatch({ type: SET_RATES, payload: res.data.rates });
    } catch {
      showNotification({
        type: 'danger',
        message: 'Error fetching rates',
      });
    }
  };

  const setFromCurrency = (payload: CURRENCY) => {
    dispatch({ type: SET_FROM_CURRENCY, payload });
  };

  const setToCurrency = (payload: CURRENCY) => {
    dispatch({ type: SET_TO_CURRENCY, payload });
  };

  const setFromAmount = (payload: string) => {
    dispatch({ type: SET_FROM_AMOUNT, payload });
  };

  const setToAmount = (payload: string) => {
    dispatch({ type: SET_TO_AMOUNT, payload });
  };

  const exchange = () => {
    dispatch({ type: EXCHANGE });
  };

  return { fetchRates, setFromCurrency, setToCurrency, setFromAmount, setToAmount, exchange };
};
