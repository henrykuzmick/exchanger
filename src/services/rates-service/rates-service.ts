import axios from 'axios';

export const ratesService = {
  fetchRates: () => {
    if (process.env.REACT_APP_DEV_MODE) {
      return Promise.resolve({
        data: {
          rates: {
            EUR: 0.909069,
            GBP: 0.770194,
            USD: 1,
          },
        },
      });
    }

    return axios.get(`https://openexchangerates.org/api/latest.json?app_id=${process.env.REACT_APP_API_KEY}`);
  },
};
