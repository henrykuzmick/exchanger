import { CURRENCY, Rates } from '../../redux';

type Props = {
  fromCurrency: CURRENCY;
  toCurrency: CURRENCY;
  rates: Rates | null;
  amount: number;
  decimals?: number;
};

export const converter = ({ fromCurrency, toCurrency, amount, rates, decimals = 2 }: Props) => {
  if (!rates) {
    return 0;
  }

  const base = rates[CURRENCY.USD] / rates[fromCurrency];
  const result = amount * rates[toCurrency] * base;
  const round = Math.pow(10, decimals);
  return Math.floor(result * round) / round;
};
