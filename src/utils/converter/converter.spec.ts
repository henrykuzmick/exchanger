import { converter } from './converter';
import { CURRENCY } from '../../redux';
import { rates } from '../../test/mocks';

describe('Converter', () => {
  it('returns 0 if no rates were provided', () => {
    const result = converter({ fromCurrency: CURRENCY.USD, toCurrency: CURRENCY.GBP, amount: 100, rates: null });
    expect(result).toEqual(0);
  });

  it('returns converted value', () => {
    const result = converter({ fromCurrency: CURRENCY.USD, toCurrency: CURRENCY.GBP, amount: 100, rates });
    expect(result).toEqual(300);
  });

  it('rounds to two decimals by default', () => {
    const result = converter({ fromCurrency: CURRENCY.GBP, toCurrency: CURRENCY.USD, amount: 100, rates });
    expect(result).toEqual(33.33);
  });

  it('rounds to the given number of decimals', () => {
    const result = converter({
      fromCurrency: CURRENCY.GBP,
      toCurrency: CURRENCY.USD,
      amount: 100,
      rates,
      decimals: 5,
    });
    expect(result).toEqual(33.33333);
  });
});
