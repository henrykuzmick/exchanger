import getSymbolFromCurrency from 'currency-symbol-map';
import { CURRENCY } from '../../redux';

type Options = {
  thousandSeparator?: boolean;
  currency?: CURRENCY;
  fixed?: number;
  maxLength?: number;
};

const removeNonNumeric = (value: string) => {
  return value.replace(/[^0-9.]/g, '');
};

const removeOtherDots = (value: string) => {
  return value.replace(/^([^.]*\.)(.*)$/, (_a, b, c) => b + c.replace(/\./g, ''));
};

const toFixed = (value: string, fixed: number) => {
  const i = value.indexOf('.');
  return i >= 0 ? value.substr(0, i + 1 + fixed) : value;
};

const removeZeros = (value: string) => {
  const replace = value.match(/^0+(\.|$)/) ? '0' : '';
  return value.replace(/^0+/, replace);
};

const toMaxLength = (value: string, length: number) => {
  const sides = value.split('.');
  sides[0] = sides[0].substr(0, length);
  return sides.join('.');
};

export const numericTransformer = {
  transform: (
    value: string | number,
    { thousandSeparator = true, currency, fixed = 2, maxLength = 9 }: Options = {},
  ) => {
    let result = value.toString();

    result = removeNonNumeric(result);
    result = removeOtherDots(result);
    result = removeZeros(result);
    result = toFixed(result, fixed);
    result = toMaxLength(result, maxLength);

    if (result.trim() === '') {
      return result;
    }

    if (thousandSeparator) {
      result = result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    if (currency) {
      const prefix = getSymbolFromCurrency(currency);
      result = `${prefix}${result}`;
    }

    return result;
  },

  parse: (value: string) => {
    const reg = /\d+\.?/g;
    const res = value.match(reg);
    if (!res) {
      return 0;
    }

    return parseFloat(res.join(''));
  },
};
