import { numericTransformer } from './';
import { CURRENCY } from '../../redux';

describe('Numeric Transformer', () => {
  describe('#transform', () => {
    it('returns an empty string if value is empty', () => {
      expect(numericTransformer.transform('', { currency: CURRENCY.USD })).toEqual('');
      expect(numericTransformer.transform('dsf', { currency: CURRENCY.USD })).toEqual('');
    });

    it('only returns numbers and dots', () => {
      expect(numericTransformer.transform('123', { thousandSeparator: false })).toEqual('123');
      expect(numericTransformer.transform('123.33', { thousandSeparator: false })).toEqual('123.33');
      expect(numericTransformer.transform(123.33, { thousandSeparator: false })).toEqual('123.33');
      expect(numericTransformer.transform('words12333aaa', { thousandSeparator: false })).toEqual('12333');
    });

    it('only returns the first dot', () => {
      expect(numericTransformer.transform('123.1.1', { thousandSeparator: false })).toEqual('123.11');
    });

    it('only returns two numbers after the dot by default', () => {
      expect(numericTransformer.transform('123.1234', { thousandSeparator: false })).toEqual('123.12');
    });

    it('returns provided number of decimals', () => {
      expect(numericTransformer.transform('123.123456789', { fixed: 5, thousandSeparator: false })).toEqual(
        '123.12345',
      );
    });

    it('removes zeros before a number with no decimals', () => {
      expect(numericTransformer.transform('0')).toEqual('0');
      expect(numericTransformer.transform('0123')).toEqual('123');
      expect(numericTransformer.transform('0000123')).toEqual('123');
    });

    it('can not have more than one zero before the dot', () => {
      expect(numericTransformer.transform('0001')).toEqual('1');
      expect(numericTransformer.transform('0000.12')).toEqual('0.12');
      expect(numericTransformer.transform('0123.1')).toEqual('123.1');
    });

    it('shortens the string to maxLength', () => {
      expect(numericTransformer.transform('123456789', { maxLength: 5, thousandSeparator: false })).toEqual('12345');
      expect(numericTransformer.transform('123456789', { maxLength: 5 })).toEqual('12,345');
      expect(numericTransformer.transform('12345.99', { maxLength: 5 })).toEqual('12,345.99');
      expect(numericTransformer.transform('123456.99', { maxLength: 5 })).toEqual('12,345.99');
    });

    it('adds thousand separator commas', () => {
      expect(numericTransformer.transform('123')).toEqual('123');
      expect(numericTransformer.transform('1234')).toEqual('1,234');
      expect(numericTransformer.transform('123456')).toEqual('123,456');
      expect(numericTransformer.transform('1234567')).toEqual('1,234,567');
    });

    it('adds a prefix', () => {
      expect(numericTransformer.transform('123', { currency: CURRENCY.USD })).toEqual('$123');
      expect(numericTransformer.transform('1234567', { currency: CURRENCY.USD })).toEqual('$1,234,567');
      expect(numericTransformer.transform('1234567.99', { currency: CURRENCY.USD })).toEqual('$1,234,567.99');
      expect(numericTransformer.transform(1234567.99, { currency: CURRENCY.USD })).toEqual('$1,234,567.99');
    });
  });

  describe('#parse', () => {
    it('Returns a parsed float as a string', () => {
      expect(numericTransformer.parse('123')).toEqual(123);
      expect(numericTransformer.parse('words $123,456.99 words')).toEqual(123456.99);
      expect(numericTransformer.parse('$123,456.99')).toEqual(123456.99);
    });
  });
});
