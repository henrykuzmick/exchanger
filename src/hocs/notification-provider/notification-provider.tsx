import React, { createContext, useState } from 'react';
import { Notification, NotificationProps } from '../../components';

interface NotificationContextProps {
  notification: NotificationProps | null;
  showNotification: (notification: NotificationProps) => void;
  hideNotification: () => void;
}

const initialNotificationState: NotificationContextProps = {
  notification: null,
  showNotification: () => null,
  hideNotification: () => null,
};

export const NotificationContext = createContext<NotificationContextProps>(initialNotificationState);

export const NotificationProvider: React.FC = props => {
  const [notification, setNotification] = useState<NotificationProps | null>(null);

  const showNotification = (notification: NotificationProps) => {
    setNotification(notification);
  };

  const hideNotification = () => {
    setNotification(null);
  };

  const renderNotification = () => {
    if (!notification) {
      return null;
    }

    return <Notification {...notification} />;
  };

  const contextValue: NotificationContextProps = {
    notification,
    showNotification,
    hideNotification,
  };

  return (
    <NotificationContext.Provider value={contextValue}>
      {props.children}
      {renderNotification()}
    </NotificationContext.Provider>
  );
};
