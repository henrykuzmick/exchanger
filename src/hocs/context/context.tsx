import React, { createContext, useReducer } from 'react';
import { reducer, initialState, State, Action } from '../../redux';

type Props = {
  state?: State;
};

interface ContextProps {
  state: State;
  dispatch: React.Dispatch<Action>;
}

const initialContextState: ContextProps = {
  state: initialState,
  dispatch: () => null,
};

export const ReduxContext = createContext<ContextProps>(initialContextState);

export const ContextProvider: React.FC<Props> = React.memo(props => {
  const [state, dispatch] = useReducer(reducer, props.state || initialState);

  const contextValue: ContextProps = {
    state,
    dispatch,
  };

  return <ReduxContext.Provider value={contextValue}>{props.children}</ReduxContext.Provider>;
});
