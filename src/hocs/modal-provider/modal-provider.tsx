import React, { createContext, useState } from 'react';
import { ConfirmModal, ConfirmModalProps } from '../../components';

type ConfirmModalType = {
  type: 'confirm';
  props: ConfirmModalProps;
};

type Modal = ConfirmModalType;

interface ModalContextProps {
  modal: Modal | null;
  showModal: (modal: Modal) => void;
  hideModal: () => void;
}

const initialModalState: ModalContextProps = {
  modal: null,
  showModal: () => null,
  hideModal: () => null,
};

export const ModalContext = createContext<ModalContextProps>(initialModalState);

export const ModalProvider: React.FC = props => {
  const [modal, setModal] = useState<Modal | null>(null);

  const showModal = (modal: Modal) => {
    setModal(modal);
  };

  const hideModal = () => {
    setModal(null);
  };

  const renderModal = () => {
    if (!modal) {
      return null;
    }

    switch (modal.type) {
      case 'confirm': {
        return <ConfirmModal {...modal.props} />;
      }
      default: {
        return null;
      }
    }
  };

  const contextValue: ModalContextProps = {
    modal,
    showModal,
    hideModal,
  };

  return (
    <ModalContext.Provider value={contextValue}>
      {props.children}
      {renderModal()}
    </ModalContext.Provider>
  );
};
