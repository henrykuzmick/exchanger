export const DOWN_KEY = 40;
export const UP_KEY = 38;
export const ESC_KEY = 27;
export const ENTER_KEY = 13;
export const RATE_FETCH_INTERVAL = 10000;
