# Money Exchanger 5000

## 1. To begin clone this repo and run `npm install`
## 2. Create a `.env` file at the root and add `REACT_APP_API_KEY=your_openexchangerates_api_key`
## 3. Run `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

You can also set `REACT_APP_DEV_MODE=true` in your `.env` file to get mocked data instead of using the actual API.


## Stack

I'm using a standard React + Redux setup. However, I skipped the Redux library and implemented my own redux.
I believe that with the introduction of React Context and Hooks, the need for a 3rd party library dropped quite significantly. 

This is especially true for tiny apps like this that would have no use of the ecosystem that the redux library could provide.

The pattern for managing state is still the same, but with a couple of benefits:
1. Handling types is significantly easier.
2. No need for redux-thunk or similar libs for async actions. 
3. Lots of flexibility setting up custom hooks for both dispatching actions and reading the state.
4. The entire setup is easier to reason about, helpful when introducing the concept of redux to junior devs.
5. One less dependency.

The redux library also tends to incentivize keeping everything in a single store. I'd argue that storing everything from API fetched data to the state of modals, notifications, etc. is generally a bad pattern. I added a modal and a notification in this project to highlight how different contexts can be used instead.

Not only does an approach like this keep your state cleaner and easier to reason about, but it also is much easier to reuse. 
For example - you may very well want to reuse the entire logic of your modals or notifications in another micro-app.
Having separate contexts makes it incredibly easy to do so.


## Testing:

To run jest tests run:
### `npm test`

I'd consider static type checking with Typescript the first part of testing. 

I'm using jest and react-testing-library. 
The setup is fairly straight-forward, there's a test-wrapper component that helps with mounting and a state-builder class that helps with setting up initial conditions.

Each component has a couple of unit tests that are performed with the help of a driver to keep specs cleaner.

I have tests for reducer and utility helpers, but I generally don't write tests for hooks, as they should only be used in components, and each component should cover those use-cases. 

## More Testing:

to run cypress tests:
### 1. start the server with `npm start`. Make sure `REACT_APP_DEV_MODE` in your `.env` file is falsy.
### 2. run `npm run cy:run`

I'm a big fan of cypress and would generally choose to write more integration tests with it and fewer jest tests.
In most cases, a couple of well-written integration tests can cover tens of unit tests.

## Styling

The layout is inspired by the revolut-business web app exchanger.

I'm using css-modules, with a few variables to restrict colors and spacing. 
Having a set amount of variables helps with keeping apps looking balanced even when working without a design, a style-guide or a component library.

I generally prefer Emotion to css-modules, but I felt like a simple project like this would work better with something more casual.


## Misc Logic

I focused on the front-end part of the app. Because there are no servers or persistence, some parts are contrived and would behave differently in a real-world app.

1. The reducer has some initial values. Those include pockets and their values. IRL these would start with a `null` value, be fetched from the server and set, similar to how rates are fetched. I'd show the big loader component while this data is loading and display an error component if fetching failed.
2. The exchange logic is done in the reducer. IRL it would be done on server-side. Pockets would be refetched and updated to reflect the correct current state. Error toast messages would serve as a guide for unsuccessful calls. 
